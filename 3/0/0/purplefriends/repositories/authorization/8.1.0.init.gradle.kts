import eu.purplefriends.libraries.gradle.repositories.authorization.gitlab.GitlabAuthorizationToken
import eu.purplefriends.libraries.gradle.repositories.authorization.PurpleFriendsRepositoryAuthorizer.authorizePurpleFriendsRepositories
import eu.purplefriends.libraries.gradle.repositories.authorization.gitlab.GitlabAuthorizationTokenExtensions.authorize

initscript {
    repositories {
        maven("https://gitlab.com/api/v4/projects/57554022/packages/maven")
        maven("https://gitlab.com/api/v4/projects/58049961/packages/maven")
    }
    dependencies {
        classpath("eu.purplefriends.libraries.gradle.repositories.authorization.plugins.initialization:eu.purplefriends.libraries.gradle.repositories.authorization.plugins.initialization.gradle.plugin") {
            version {
                strictly("8.1.0")
            }
        }
    }
}

authorizePurpleFriendsRepositories(
    purplefriendsCentralMavenRepository = Action<MavenArtifactRepository> {     
        logger.error("tag:afaksdhfjkahsdgfjkashgdkjfhas")
        GitlabAuthorizationToken.JobToken.DEFAULT?.authorize(this)
    },
    purplefriendsGradleRepositoryAuthorizationMavenRepository = null,
    purplefriendsMavenRepositoryRegistryRepository = null,
)