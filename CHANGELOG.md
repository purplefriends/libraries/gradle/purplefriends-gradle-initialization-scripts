
# 3.0.0
 * make authorization scripts versionable

# 2.1.0
 * change authorization script to be conform with gradle authorization plugin version 8.0.0

# 2.0.0
 * change authorization script to be conform with version 7.0.0
 
# 1.1.0
 * add ramdisk script
 
 # 1.0.0
 * add authorization script