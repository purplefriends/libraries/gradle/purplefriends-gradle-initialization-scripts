// create ramdisk for mac:
// diskutil erasevolume HFS+ 'RAMDiskName' `hdiutil attach -nomount ram://XXXXXX`
val ramDiskVolume = "/Volumes/RAMDiskName"
gradle.projectsLoaded {
    rootProject.allprojects {
        this.layout.buildDirectory.set(File("$ramDiskVolume/gradle/build/${project.path.substring(1)}"))
    }
}